# NF18 - CV Theque

* Lien : [https://gitlab.utc.fr/rzunigat/nf18-cv-theque.git](https://gitlab.utc.fr/rzunigat/nf18-cv-theque.git)
* Étudiant: *Rodrigo Uriel Zuñiga Tellez*
* COMMANDS pour insérer dans les tableaux: **commandes.txt**

* Le fichier vctheque_nc.markdown contient le contexte, les objectifs, les livrables et le détail du système concernant la gestion d'espaces de coworking.
* Le fichier cvtheque_MCD.plantuml contient la représentation sous forme de diagramme UML des données de l'exercice avec les différentes classes, relations et attributs.
* Pour importer la base de donnés: \i cvtheque_SQL.sql

![Alt text](uml.png?raw=true "UML - Tresorerie")


